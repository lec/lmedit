/**
 * Created by gaocheng on 2016/11/28.
 */
//无刷新文件上传
var upf={
    dug:false,
    fnum:0,
    fbot:"_upf_",
    fact:"",//要提交的网址。
    retdata:"",
    istru:false,//是否正在传输
    cusfunc:function(){},
    init:function(fact)
    {
        upf.fact=fact;
        return upf;
    },
    clk:function(callback){
        upf.fnum++;
        //创建容器
        var tmpbox=document.createElement('div');
        tmpbox.id='box'+upf.fbot+upf.fnum;
        tmpbox.className='upfbox';
        document.getElementsByTagName('body')[0].appendChild(tmpbox);
        thisbox=document.getElementById(tmpbox.id);
        if(upf.dug)
        {

        }else{
            thisbox.setAttribute("style",'display:none');
        }

        //创建iframe
        var tmpiframe=document.createElement('iframe');
        tmpiframe.name='iframe'+upf.fbot+upf.fnum;
        thisbox.appendChild(tmpiframe);
        thisiframe=document.getElementById(tmpiframe.id);

        //创建form
        var tmpform=document.createElement('form');
        tmpform.id='form'+upf.fbot+upf.fnum;
        tmpform.method='post';
        tmpform.enctype="multipart/form-data";
        tmpform.action=upf.fact;
        tmpform.target=tmpiframe.name;
        thisbox.appendChild(tmpform);
        thisform=document.getElementById(tmpform.id);
        //创建fileinput
        var fileinput=document.createElement('input');
        fileinput.type='file';

        fileinput.name='file';
        fileinput.id='id'+upf.fbot+upf.fnum;
        thisform.appendChild(fileinput);
        thisfileinput=document.getElementById(fileinput.id);
        $(document).on('change','#'+fileinput.id,function(){
            //添加图片替换
            callback(thisbox.id);
            //表单提交
            thisform.submit();
            //设置正在传输的状态
            upf.istru=true;
        });

        //创建hidden 发送当前form信息
        var hiddeninput=document.createElement('input');
        hiddeninput.type='hidden';
        hiddeninput.name='exinfo';
        hiddeninput.value=thisbox.id;
        thisform.appendChild(hiddeninput);

        thisfileinput.click();
    },
    //callback
    /*
    * 回调函数。
    * 
    * */
    cbfunc:function(){
        upf.istru=false;
        upf.cusfunc();
    }
};
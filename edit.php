<?php
//\c\tool::dug($it_info);
//\c\tool::dug($links);
ini_set("display_errors",'On');
?>
<!doctype html>
<html lang=zh-cn>
<head>
    <meta charset="UTF-8">
    <title>Lmedit</title>


    <link rel="stylesheet" href="/css/bootstrap-3.3.0-dist/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/bootstrap-3.3.0-dist/dist/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="/css/font-awesome-4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css"/>


    <script src="/js/jqm1.11.2.js"></script>
    <script src="/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
    <script src="/css/bootstrap-3.3.0-dist/dist/js/bootstrap.min.js"></script>
    <script src="/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
<!--    <link href="index.css" rel="stylesheet">-->
    <script src="/lmedit.js"></script>

</head>
<body>

<div class="container">
    <style type="text/css">
        .p0 {
            padding: 0;
        }

        .m0 {
            margin-left: -10px;
            margin-right: -10px;
        }

        .bar_fixed {
            position: fixed;
            top: 0;
        }
    </style>
    <div class="row m0">
        <h1>编辑</h1>
        <hr/>
        <div>
            <form action="" method="post" onsubmit="_edit_('#maincont')">
                <script type="text/javascript">
                    $(document).on('shown.bs.tab','a[data-toggle="tab"]', function () {
                        var t=$(this).attr('aria-controls');
                        $('#editcht').val(t);
                    })
                </script>
                <div class="tab-content tab-mypanel">
                <div id="_newarticle" class="tab-pane" role="tabpanel">
                    <div class="form-group">
                        <label for="">标题[如果没有标题就不填]</label>
                        <input type="text" name="maintitle" value="" class="form-control"
                               placeholder="如果没有标题就不填"/>

                        <label for="">内容</label>
                        <input type="hidden" name="maincont" id="maincont" value=""/>
                        <!--编辑器-->

                        <div>
                            <img style="display: none" src="/img/loading.gif" alt="预载入loading图片"/>
                            <script type="text/javascript">
                                $(window).bind("beforeunload", function () {
                                    return '刷新或者退出会丢失当前编辑的内容，您确定吗？';
                                });
                                function _formpreview_(o) {
                                    $('#submit_type').val('pre');
                                    $(o).parents("form").first().attr('target', '_blank');
                                    var m = $(o).parents("form").first().submit();
                                }
                                function _formsubmit_(o) {
                                    cov.altstr = '<img src="/img/loading.gif" /><div>正在载入...</div>';
                                    cov.setcov();
                                    //测定网络是否通常
                                    $.ajax({
                                        type: "get",
                                        url: 'url',
                                        data: {islogin: 1},
                                        dataType: "text",
                                        success: function (d) {
                                            if (d == '1001') {
                                                alert('登录超时，请在其他页面重新登陆');
                                                cov.remcov();
                                                return false;
                                            } else if (d == '1010') {
                                                $('#submit_type').val('pub');//设置表单为发布而不是预览
                                                $(o).parents("form").first().attr('target', '_self');//设置表单在本页面提交
                                                var m = $(o).parents("form").first().submit();//提交表单
                                            }
                                        },
                                        error: function () {
                                            alert('网络未连接');
                                            cov.remcov();
                                            return false
                                        }
                                    });
                                }
                                function _edit_(id) {
                                    var tmpstr = $('#_bweditor').html();
//                            alert(tmpstr);
                                    tmpstr = window.btoa(window.encodeURIComponent(tmpstr));
                                    $(id).val(tmpstr);
                                    $(window).unbind("beforeunload");
                                    return true;
                                }
                                //自动保存处理
                                //自动保存任务安排
                                autosave = {
                                    divid: "_bweditor",
                                    mcbak: '',//上一次提交的内容
                                    mc: '',//当前获取的内容
                                    task: function () {
                                        setInterval(function () {
                                            autosave.run();
                                        }, 5000);
                                    },
                                    run: function () {
                                        autosave.mc = $("#" + autosave.divid).html();
                                        len = (autosave.mc.length) / 3;
//                                $('#autosavemsg').append('|'+len);
                                        if (len > 50000) {
                                            $('#autosavemsg').html('|您输入的内容太多了，超过自动保存的范围[' + len + ']');
                                            return false;
                                        }

                                        if (autosave.mcbak == autosave.mc) {
                                            return false;
                                        } else {
                                            autosave.mcbak = autosave.mc;
                                        }
                                        $.ajax({
                                            type: "post",
                                            url: '',
//                                    data:"maincont="+autosave.mc,
                                            data: {maincont: autosave.mc},
                                            dataType: "text",
                                            success: function (d) {
                                                $('#autosavemsg').html(d);
                                            }
                                        });
                                    }
                                }
//                                autosave.task();

                                $(document).scroll(
                                    function () {
                                        var baroa = $('#_bweidtor_bar_a');
                                        var baro = $('#_bweditor_bar');
                                        var of = baroa.offset();
//                                baroa.html('top:'+of.top+'left:'+of.left+"scrollTop"+document.body.scrollTop);
                                        if (document.body.scrollTop > of.top) {
                                            baro.css('position', 'fixed');
                                            baro.css('top', '0');
                                        } else {
                                            baro.css('position', '');
                                            baro.css('top', '');
                                        }
                                    }
                                );

                            </script>

                            <!--工具条-->
                            <div id="_bweidtor_bar_a"></div>
                            <div class="bweditor-btn-toolbar" data-role="editor-toolbar" data-target="#_bweditor" id="_bweditor_bar">
                                <a class="btn btn-default" title="插入图片" id="_insert_img">
                                    <i class="fa fa-image"></i> 图片
                                </a>
                                <a class="hidden-xs btn btn-default" data-edit="bold" title="加粗 (Ctrl/Cmd+B)">
                                    <i class="fa fa-bold"></i>
                                </a>
                                <div class="btn-group">
                                    <a class="hidden-xs btn btn-default dropdown-toggle" data-toggle="dropdown"
                                       title="小标">
                                        <i class="fa fa-text-height"></i>&nbsp;
                                        <b class="caret"></b>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a data-edit="FormatBlock h1"><h1>标题一</h1></a></li>
                                        <li><a data-edit="FormatBlock h2"><h2>标题二</h2></a></li>
                                        <li><a data-edit="FormatBlock h3"><h3>标题三</h3></a></li>
                                        <li><a data-edit="FormatBlock div">正文</a></li>
                                    </ul>
                                </div>
                                <a class="hidden-xs btn btn-default" data-edit="justifyleft" title="左对齐 (Ctrl/Cmd+L)"><i
                                        class="fa fa-align-left"></i></a>
                                <a class="hidden-xs btn btn-default" data-edit="justifycenter"
                                   title="居中 (Ctrl/Cmd+E)"><i
                                        class="fa fa-align-center"></i></a>
                                <a class="hidden-xs btn btn-default" data-edit="justifyright"
                                   title="右对齐 (Ctrl/Cmd+R)"><i
                                        class="fa fa-align-right"></i></a>
                                <a class="hidden-xs btn btn-default" data-edit="justifyfull" title="分散 (Ctrl/Cmd+J)"><i
                                        class="fa fa-align-justify"></i></a>

                                <a class="hidden-xs btn btn-default" data-edit="_my_addanchor" title="插入定位锚">
                                    <i class="fa fa-anchor"></i>
                                </a>
                                <a class="hidden-xs btn btn-default" data-toggle="modal" data-target="#addflash"
                                   title="插入Flash">
                                    <i class="fa fa-film"></i>
                                </a>

                                <a class="hidden-xs btn btn-default" data-edit="_my_addlink" data-txtid="_cl_text"
                                   data-toggle="modal" data-target="#addLinkModal" title="插入链接">
                                    <i class="fa fa-link"></i>
                                </a>

                                <a class="hidden-xs btn btn-default" data-edit="unlink" title="删除链接"><i
                                        class="fa fa-unlink"></i></a>
                                <a class="hidden-xs btn btn-default" data-edit="_my_cleanHtml" title="清除格式">
                                    <i class="fa fa-eraser"></i>
                                </a>
                                <a class="btn btn-default" data-edit="undo" title="取消 (Ctrl/Cmd+Z)"><i
                                        class="fa fa-undo"></i></a>
                                <a class="btn btn-default" data-edit="redo" title="重做 (Ctrl/Cmd+Y)"><i
                                        class="fa fa-repeat"></i></a>


                                <!--                        <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage"/>-->
                                <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">


                            </div>

                            <div class="modal fade" id="addLinkModal" role="dialog" aria-labelledby="myModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">添加链接</h4>
                                        </div>
                                        <div class="modal-body">
                                            链接[如http://..]：
                                            <input class="span2 form-control" placeholder="链接" type="text"
                                                   id="_cl_link"/>
                                            文字：
                                            <input class="span2 form-control" placeholder="文字" type="text"
                                                   id="_cl_text"/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal"
                                                    id="_cl_btn">添加
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!--                        添加视频-->
                            <div class="modal fade" id="addflash" role="dialog" aria-labelledby="addflash"
                                 aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">添加视频</h4>
                                        </div>
                                        <div class="modal-body">
                                            链接[如http://..]：
                                            <input class="span2 form-control" placeholder="链接" type="text"
                                                   id="_falsh_link"/>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal"
                                                    id="_addflash_btn">添加
                                            </button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!--                        添加视频-->

                            <!--        编辑区-->
                            <div id="_bweditor"><?= $itmain ?></div>
                            <div id="autosavemsg">自动保存：无</div>
                            <!--            编辑器样式-->
                            <style type="text/css">

                                #_bweditor {
                                    /*max-height: 250px;*/
                                    min-height: 250px;
                                    background-color: white;
                                    border-collapse: separate;
                                    border: 1px solid rgb(204, 204, 204);
                                    padding: 4px;
                                    box-sizing: content-box;
                                    -webkit-box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset;
                                    box-shadow: rgba(0, 0, 0, 0.0745098) 0px 1px 1px 0px inset;
                                    border-radius:0px;
                                    overflow: scroll;
                                    outline: none;
                                }

                                #voiceBtn {
                                    width: 20px;
                                    color: transparent;
                                    background-color: transparent;
                                    transform: scale(2.0, 2.0);
                                    -webkit-transform: scale(2.0, 2.0);
                                    -moz-transform: scale(2.0, 2.0);
                                    border: transparent;
                                    cursor: pointer;
                                    box-shadow: none;
                                    -webkit-box-shadow: none;
                                }

                                div[data-role="editor-toolbar"] {
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                    user-select: none;
                                }

                                .dropdown-menu a {
                                    cursor: pointer;
                                }

                                .bweditor-btn-toolbar {
                                    border: 1px solid #cccccc;
                                    padding: 5px;
                                    background-color: #ffffff;
                                    margin-left: 0px;
                                }
                            </style>
                            <!--        初始化脚本-->
                            <script type="text/javascript">
                                $(function () {
                                    function initToolbarBootstrapBindings() {
                                        $('a[title]').tooltip({container: 'body'});
                                        $('.dropdown-menu input').click(function () {
                                                return false;
                                            })
                                            .change(function () {
                                                $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
                                            })
                                            .keydown('esc', function () {
                                                this.value = '';
                                                $(this).change();
                                            });

                                        $('[data-role=magic-overlay]').each(function () {
                                            var overlay = $(this), target = $(overlay.data('target'));
                                            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
                                        });
                                        if ("onwebkitspeechchange" in document.createElement("input")) {
                                            var editorOffset = $('#_bweditor').offset();
                                            $('#voiceBtn').css('position', 'absolute').offset({
                                                top: editorOffset.top,
                                                left: editorOffset.left + $('#_bweditor').innerWidth() - 35
                                            });
                                        } else {
                                            $('#voiceBtn').hide();
                                        }
                                    };
                                    function showErrorAlert(reason, detail) {
                                        var msg = '';
                                        if (reason === 'unsupported-file-type') {
                                            msg = "Unsupported format " + detail;
                                        }
                                        else {
                                            console.log("error uploading file", reason, detail);
                                        }
                                        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
                                            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
                                    };


                                    initToolbarBootstrapBindings();
                                    $('#_bweditor').wysiwyg({fileUploadError: showErrorAlert});
                                    window.prettyPrint && prettyPrint();
                                });
                                //初始化文件上传
//                                upf.init("");
                            </script>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">保密码[最长为10，填写后则需要输入密码才能查看，不填任何人都能看]</label>
                        <div>
                            <input class="form-control" type="text" name="readcode" value=""
                                   placeholder="不填任何人都能看"/>
                        </div>
                    </div>

                </div>
                <div id="_newlink" class="tab-pane" role="tabpanel">
                    <div class="form-group">
                        <label for="">链接</label>
                        <input type="text" class="form-control" name="link" placeholder="填入要推荐的链接">
                    </div>
                    <div class="form-group">
                        <label for="">文字</label>
                        <textarea class="form-control" name="caption" id="" cols="30" rows="10" placeholder="填入链接说明文字"></textarea>
                    </div>
                </div>
                </div>
                <div class="form-group">

                    <input class="btn btn-default" type="button" onclick="_formpreview_(this)" value="预览"/>

                    <input class="btn btn-primary" type="button" onclick="_formsubmit_(this)" value="发布"/>

                </div>
                <input type="hidden" name="submit_type" id="submit_type" value="publish"/>
            </form>
        </div>

    </div>
</div>

</body>
</html>